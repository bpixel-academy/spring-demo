package net.bpixel.base.api.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import net.bpixel.base.api.exception.InvalidTokenException;
import net.bpixel.base.api.model.User;

@RunWith(MockitoJUnitRunner.class)
public class CurrentUserTest {

	@Mock
	Cache cache;

	@InjectMocks
	CurrentUser currentUser;

	@Before
	public void setUp() {
		
		MockitoAnnotations.initMocks(this);
		
		when(cache.valid(Mockito.any(String.class))).thenReturn(true);
		
		ReflectionTestUtils.setField(currentUser, "uniqueUserName", "test fake user");
		
	}

	@Test
	public void getCurrentUser() throws InvalidTokenException {

		User user = currentUser.getUserByToken("12312321");

		assertNotNull(user);
		assertNotNull(user.getName());
		assertNotNull(user.getEmail());
		assertNotNull(user.getPassword());

	}

}

package net.bpixel.base.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.bpixel.base.api.exception.InvalidTokenException;
import net.bpixel.base.api.model.User;

@Service
public class CurrentUser {
	
	@Value("${uniqueUserName}")
	private String uniqueUserName;
	
	@Autowired
	Cache cache;

	public User getUserByToken(String token) throws InvalidTokenException {
		
		if (!cache.valid(token)) {
			throw new InvalidTokenException();
		}

		return User.builder()
				.email("email@email.com")
				.name(uniqueUserName)
				.password("12321321312").build();
	}
	
}
